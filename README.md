# ion-sdk-go
this is ion golang sdk for ion-sfu

- [x] Subscribe from session
- [x] Publish webm to session
- [x] Save stream to file
- [ ] Publish audio only
- [ ] Support simulcast
- [ ] Publish media device to session
- [ ] Support ion cluster
